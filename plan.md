# Details

* Weekly Game Jam 153
    * Theme: Roll
    * https://itch.io/jam/weekly-game-jam-153
    * Deadline: 18/06/2020 19:00
* My idea
    * 2D puzzle game
    * Character is a ball that rolls (not required) through grid maze to reach target
    * Movement is block based.
    * Rolls di(c)e to determine number of steps. Then take steps one at a time.
    * Probably should add enemies
    * One big maze
        * Limited vision & small camera FOV
        * Spikes and other traps
        * Dead ends
        * Power ups? Lives/HP? 
    * Death + win screens
    * Implementing procedural generation is exceptionally unlikely, but maybe (for replayability)

# Timeline

* Day 1: Missed 
* Day 2: Missed
* Day 3: 
    * Add character
    * Add very basic level design
        * Add win condition
* Day 4: 
    * Add traps
    * Refine character motion 
    * Refine level design
* Day 5:
    * Add animations
    * Add screens
    * Refine level design
* Day 6:
    * Finalise level design
* Day 7: Polish and submit