﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    Rigidbody2D rb2d;

    

    public float speed = 5f;

    private int moves = 0;
    public int Moves
    {
        set => moves = value;
    }
    public Text textMoves;
    public bool canMove = false;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        textMoves.text = "Moves Left: " + moves;
        
        if(moves > 0)
        {
            
            Vector3 move = new Vector3(0f, 0f, 0f);

            if(Input.GetKeyDown(KeyCode.W))
            {
                move.y = 1f;
            }
            else if(Input.GetKeyDown(KeyCode.S))
            {
                move.y = -1f;
            }
            else if(Input.GetKeyDown(KeyCode.D))
            {
                move.x = 1f;
            }
            else if(Input.GetKeyDown(KeyCode.A))
            {
                move.x = -1f;
            }
            
            
            if ((move.x + move.y) != 0)
            {
                Debug.Log("Player postion: " + transform.position);
                transform.Translate(move);
                moves -= 1;
            }
        }

        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.gameObject);
    }
}
