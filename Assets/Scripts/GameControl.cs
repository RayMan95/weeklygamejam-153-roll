﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    
    public static GameControl instance = null;
    private int turn = 0;
    private int action = 0;
    public int Action
    {
        get => action;
    }

    public Ball player;

    public GameObject canvasHUD;
    public GameObject canvasRoll;

    public Text textMoves;
    public Text textTurn;

    public GameObject textRoll;
    public Text textRolled;

    private bool rolledDice = false;
    private bool waitingForRoll = false;
    private bool waitingForAccept = false;

    void Awake()
    {
        if(instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        
        // Application.targetFrameRate = 30;
    }

    
    void Update()
    {
        
        if (action == 0)
        {
            if(waitingForRoll)
            {
                if(Input.GetKeyDown(KeyCode.R))
                {
                    textRoll.SetActive(false);
                    Roll();
                    waitingForRoll = false;
                }
                else
                {
                    return;
                }
            }
            else if (waitingForAccept)
            {
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                {                    
                    action = 1;
                    canvasRoll.SetActive(false);
                    waitingForAccept = false;
                    player.canMove = true;                    
                }
                else
                {
                    return;
                }
            }
            else
            {
                canvasRoll.SetActive(true);
                turn += 1;
                textTurn.text = "Turn: " + turn;

                waitingForRoll = true;    
            }
            
        }
        else if (action == 1)
        {
            // Move();
        }
    }

    void Roll()
    {

        int die1 = Random.Range(1,6);
        player.Moves = die1;
        textRoll.SetActive(false);
        textRolled.gameObject.SetActive(true);
        textRolled.text = "You got:\n" + die1;
        waitingForAccept = true;

    }

    
}
